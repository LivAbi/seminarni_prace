//
//  main.cpp
//  KRALICI2
//
//  Created by Olivie Abigail Franklová on 29.10.18.
//  Copyright © 2018 Olivie Abigail Franklová. All rights reserved.
//

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <nanogui/nanogui.h>
nanogui::app obrazovka("Genetika v praxi",1400, 700, 0.8f);
nanogui::Window *globalni_okno = NULL;
#define MAX_KRALIKU 30
int  pocet_her = 1;
//prototyp funkce vytvor_hlavni_okno
void vytvor_hlavni_okno(nanogui::Screen * obrazovka);
// struktura obsahujici gen urcite vlastnosti kralika  a jeji skutecnou viditelnou vlastnost
typedef struct GEN{
    int gen; // nabyva hodnot 11(oba geny jsou dominantni), 10 nebo 1 (jeden gen je dominantni druhy recesivni)
            // 0 (oba genz jsou recesivni)
    int vzhled; // to jak se geny projevi  skutecne
}GEN;

// struktura KRALIK obsahuje vsechn vlastnosti kralika reprezentovane strukturou GEN az na pohlavi
typedef struct KRALIK{
    GEN barva;
    GEN oci;
    GEN usi;
    GEN ocasek;
    GEN srst;
    int pohlavi;// 0 je samec, 1 je samice
}KRALIK;
typedef struct PAR{
    KRALIK samec;
    KRALIK samice;
    bool mame_samce;
    bool mame_samici;
}PAR;
typedef struct FARMA {
    KRALIK kralici[MAX_KRALIKU];
    int pocetK;
}FARMA;
typedef struct HRA {
    FARMA farma;
    KRALIK cil;
    int generace;
    int obtiznost;
}HRA;
typedef struct OVER{
    int otec = -1;
    int matka = -1;
    bool uspech = false;
}OVER;
typedef struct OVEROVAC{
    OVER barva;
    OVER oci;
    OVER usi;
    OVER ocasek;
    OVER srst;
    bool overeno = false;
}OVEROVAC;
//globalni promena sveta
HRA SVET;
PAR globalni_par;
// funkce zjisteni_vzhledu zjisti podle genu jakou vlastnost gen predstavuje recesivni vlastnost pouze kdyz jsou oba geny recesivni
// vraci vzhled potomka
int zjisteni_vzhledu(const int & gen){
    if (gen == 0) return 0;
    else          return 1;
}
// funkce zjisteni_genu zjisti pomoci genu matky a otce jake geny bude mit potomek, pricemz vola funkci, ktera zjisti vzhled potomka
// vraci gen potomka
GEN zjisteni_genu(const GEN & otec, const GEN & matka){
    int novy[4] = {1,0,1,0};
    int i;
    GEN potomek;
    if (otec.gen == 11){
        novy[1] = 1;
        novy[3] = 1;
    }
    else if (otec.gen == 0){
        novy[0] = 0;
        novy[2] = 0;
    }
    if (matka.gen == 10 || matka.gen == 1){
        for (i = 0; i < 2; i++){
            novy[i] = novy[i] + 10;
        }
    }
    else if (matka.gen == 11){
        for (i = 0; i < 4; i++){
            novy[i] = novy[i] + 10;
        }
    }
    i = (rand() % 4);
    potomek.gen = novy[i];
    potomek.vzhled = zjisteni_vzhledu(potomek.gen);
    return potomek;
}

// vola funkci zjisteni_genu na kazdou vlastnost kralika
// vraci potomka typu KRALIK
KRALIK rozmnozeni (const KRALIK & otec, const KRALIK  & matka){
    KRALIK potomek;
        potomek.pohlavi = (rand() % 2);
        potomek.srst = zjisteni_genu(otec.srst, matka.srst);
        potomek.ocasek = zjisteni_genu(otec.ocasek, matka.ocasek);
        potomek.usi = zjisteni_genu(otec.usi, matka.usi);
        potomek.oci = zjisteni_genu(otec.oci, matka.oci);
        potomek.barva = zjisteni_genu(otec.barva, matka.barva);
    
    
    return potomek;
}
// funkce nahodny_gen vytvori strukturu GEN s nahodnou hodnotou genu a k ni nalezicimu vzhledu
GEN nahodny_gen(){
    GEN novy;
    int gen[3] = {0, 10, 11};
    novy.gen = gen[(rand() % 3)];
    novy.vzhled = zjisteni_vzhledu(novy.gen);
    return(novy);
}
// funkce nulovy_gen nastavi hodnotu genu na recesivni
GEN nulovy_gen(){
    GEN novy;
    novy.gen = 0;
    novy.vzhled = 0;
    return(novy);
}
// funkce nahodny_kralik vytvori nahodneho kralika
// vyuciva funkci nahodny_gen ktera nastavi kazdemu genu nahodnou hodnotu
// funkce vraci kralika
KRALIK nahodny_kralik (int obtiznost){
    KRALIK novy;
    novy.pohlavi = (rand() % 2);
    novy.srst = nahodny_gen();
    novy.ocasek = nahodny_gen();
    novy.usi = nahodny_gen();
    novy.oci = nahodny_gen();
    novy.barva = nahodny_gen();
    if (obtiznost <= 2){
        novy.oci = nulovy_gen();
        novy.barva = nulovy_gen();
    }
    if (obtiznost <= 1){
         novy.usi = nulovy_gen();
    }
    return(novy);
}
// funkce over_stejnost_kraliku zjisti zda je kralik stejny jako jiny kralik
// vraci true kdyz maji stejny vyhled a false kdyz nemaji
bool jsou_kralici_stejni(const KRALIK & prvni, const KRALIK & druhy){
    if((prvni.barva.vzhled == druhy.barva.vzhled) && (prvni.srst.vzhled == druhy.srst.vzhled) && (prvni.ocasek.vzhled == druhy.ocasek.vzhled) && (prvni.usi.vzhled == druhy.usi.vzhled) && (prvni.oci.vzhled == druhy.oci.vzhled)){
        return true;
    }
    else return false;
}

// tato funkce udela retezec podle toho jak kralik vypada
std::string zapis_znaky(KRALIK kralik, const int & obtiznost){
    std::string znaky = "";
  /* if(kralik.pohlavi == 0){
        znaky.append("POHLAVÍ:samec\n");
    }
    else znaky.append("POHLAVÍ: samice \n");*/
    if(kralik.srst.vzhled == 0){
        znaky.append("SRST: krátká (R)\n");
    }
    else znaky.append("SRST: dlouhá (D)\n");
    if(kralik.ocasek.vzhled == 0){
        znaky.append("OCÁSEK: malý (R)\n");
    }
    else znaky.append("OCÁSEK: velký (D)\n");
    //kdyz obtiznost = 1 tak se tato cast nevypise
    if(kralik.usi.vzhled == 0 && obtiznost != 1){
        znaky.append("UŠI: krátké (R)\n");
    }
    else if(obtiznost != 1) {
        znaky.append("UŠI: dlouhé (D)\n");
    }
    //kdyz obtiznost == 2 nebo 1 tak se tato cast nevypise
    if( (kralik.oci.vzhled == 0) && (obtiznost != 1) && (obtiznost != 2)){
        znaky.append("OČI: modré (R)\n");
    }
    else if ((obtiznost != 1) && (obtiznost != 2)){
        znaky.append("OČI: černé (D)\n");
    }
    if(kralik.barva.vzhled == 0 && (obtiznost != 1) && (obtiznost != 2)){
        znaky.append("BARVA: bílá (R)\n");
    }
    else if ((obtiznost != 1) && (obtiznost != 2)){
        znaky.append("BARVA: hnědá (D)\n");
    }
    return znaky;
}
//jakmile je zmacknut kralik do globalni_par se nastavi tento kralik
void zmacknuti_kralika(KRALIK kralik){
    //samec
    if(kralik.pohlavi == 0){
        globalni_par.samec = kralik;
        globalni_par.mame_samce = true;
    }
    //samice
    else{
        globalni_par.samice = kralik;
        globalni_par.mame_samici = true;
    }
}
//presune noveho kralika na zacatek seznamu
void presun_kralika_na_zacatek(nanogui::Widget *kralik){
    nanogui::Widget *presunovac = kralik->parent();
    nanogui::ref<nanogui::Widget> hlidka_prenosu(kralik);
    presunovac->removeChild(kralik);
    presunovac->addChild(0,kralik);
}
//tato funkce dostava grupu do ktere zapise kralika, kralika ktereho ma zapsat a obtiznost hry
//vola funkci zapis_znaky, ktera vytvori retezec toho jak kralik vypada
//funkce vytvori tlacitko a vola funkci zmacknuti_kralika()
void pridej_kralika_do_okna(nanogui::Widget *kotec, KRALIK kralik, const int & obtiznost){
    std::string znaky = zapis_znaky(kralik, obtiznost);
    nanogui::Button * tlacitko = kotec->add<nanogui::Button>(znaky);
    tlacitko->setFlags(nanogui::Button::RadioButton);
    tlacitko->setCallback(
    [=]
     {
         zmacknuti_kralika(kralik);
      }
    );
    presun_kralika_na_zacatek(tlacitko);
    obrazovka.screen->performLayout();
}
// podle uzivatele program ukonci nebo vrati uzivatele zpet
void dialog_ukoncit(int index_tlacitka){
    if (index_tlacitka == 0) {
        globalni_okno->dispose();
        globalni_okno = NULL;
        obrazovka.leave();
    }
}
// podle vyberu uzivatele otevre hlavni menu nebo vrati uzivatele zpet
void dialog_prejit(int index_tlacitka){
    if (index_tlacitka == 0) {
        globalni_okno->dispose();
        globalni_okno = NULL;
        vytvor_hlavni_okno(obrazovka.screen);
    }
}
//tato funkce vrati uzivatele do hlavniho menu nebo ukonci hru
void dialog_konec_hry(int index_tlacitka){
    if (index_tlacitka == 0) {
        globalni_okno->dispose();
        globalni_okno = NULL;
        vytvor_hlavni_okno(obrazovka.screen);
    }
    else {
        globalni_okno->dispose();
        globalni_okno = NULL;
        obrazovka.leave();
    }
}
// tato funkce vytvori dialogove okno v kterem se ptame zda chceme program ukoncit
void zmacknuti_UKONCIT(void){
    nanogui::MessageDialog *dialog = new nanogui::MessageDialog(obrazovka.screen,nanogui::MessageDialog::Type::Question,"","Opravdu si přejete ukončit tuto aplikaci?","ANO", "NE", true);
    dialog->setCallback(dialog_ukoncit);
    obrazovka.screen->performLayout();
}
//tato funkce vytvori dialogove okno v kterem se ptame zda chceme prejit do hlavniho menu
void zmacknuti_MENU(void){
    nanogui::MessageDialog *dialog = new nanogui::MessageDialog(obrazovka.screen,nanogui::MessageDialog::Type::Question,"","Opravdu si přejete přejít do hlavního menu ? V chovu těchto králíků nebude možné pokračovat","ANO", "NE", true);
    dialog->setCallback(dialog_prejit);
    obrazovka.screen->performLayout();
}
//pokud hrac vyhral zobrazi se mu dialog zda chce hrat znovu
void Dialog_uspechu(){
    nanogui::MessageDialog *dialog = new nanogui::MessageDialog(obrazovka.screen,nanogui::MessageDialog::Type::Question,"VYHRÁLI JSTE!"," Chcete hrát znovu?","ANO", "NE", true);
    dialog->setCallback(dialog_konec_hry);
    obrazovka.screen->performLayout();
}
void Dialog_neuspechu(){
    nanogui::MessageDialog *dialog = new nanogui::MessageDialog(obrazovka.screen,nanogui::MessageDialog::Type::Question,"PROHRALI JSTE!"," Zaplnili jste celou farmu. Chcete hrát znovu?","ANO", "NE", true);
    dialog->setCallback(dialog_konec_hry);
    obrazovka.screen->performLayout();
}
//pokud je zmacknuto tlacitko na pareni zjisti se zda jsou vybrani dva kralici
//pokud ano pak jsou spareni a ukaze se novi kralik
void zmacknuti_ulozit(nanogui::Widget*holky, nanogui::Widget*kluci, const HRA  & svet){
    if((globalni_par.mame_samce == false) ||(globalni_par.mame_samici == false) ){
       new nanogui::MessageDialog(obrazovka.screen,nanogui::MessageDialog::Type::Warning,"","Nejsou vybráni dva králíci pro páření je zapotřebí 2 králíků!","OK");
        return;
    }
    else{
        KRALIK potomek = rozmnozeni (globalni_par.samec, globalni_par.samice);
        SVET.farma.kralici[SVET.farma.pocetK] = potomek;
        SVET.farma.pocetK ++;
        bool uspech = jsou_kralici_stejni(potomek,svet.cil);
        if(potomek.pohlavi == 1){
            pridej_kralika_do_okna(holky,potomek, svet.obtiznost);
            
        }
        else{
            pridej_kralika_do_okna(kluci,potomek, svet.obtiznost);
        }
        if(uspech == true){
            Dialog_uspechu();
        }
        else if(SVET.farma.pocetK >= MAX_KRALIKU){
            Dialog_neuspechu();
        }
    }
    
}
//nakonfiguruj_kotec da vlastnosti kotci
void nakonfiguruj_kotec(nanogui::Widget*kotec, std::string nadpis){
    nanogui::Label *nadpis_kotce = kotec->add<nanogui::Label>(nadpis);
    nadpis_kotce->setFontSize(30);
}
//napln_kotce vytvori posuvny panel pro oba dva kotce a vlozi do kotcu kraliky
void napln_kotce(nanogui::Widget*holky, nanogui::Widget*kluci, const HRA & svet){
    nanogui::Button * ulozit = globalni_okno->add<nanogui::Button>("Spářit");
    nakonfiguruj_kotec(kluci,"SAMEČCI");
    nakonfiguruj_kotec(holky,"SAMIČKY");
    nanogui::Layout * zarovnani_kotcu = new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Minimum,0,5);
    nanogui::VScrollPanel *panelK = kluci->add<nanogui::VScrollPanel>();
    panelK->setFixedHeight(250);
    nanogui::VScrollPanel *panelH = holky->add<nanogui::VScrollPanel>();
    panelH->setFixedHeight(250);
    nanogui::Widget *kotecK = panelK->add<nanogui::Widget>();
    nanogui::Widget *kotecH = panelH->add<nanogui::Widget>();
    kotecK->setLayout(zarovnani_kotcu);
    kotecH->setLayout(zarovnani_kotcu);
    ulozit->setCallback([=]{zmacknuti_ulozit(kotecH,kotecK,svet);});
    for(int i = 0; i < svet.farma.pocetK; i++){
        if(svet.farma.kralici[i].pohlavi == 1){
            //znaky = zapis_znaky(farma.kralici[i], obtiznost);
            // holky->add<nanogui::Label>(znaky);
            pridej_kralika_do_okna(kotecH,svet.farma.kralici[i], svet.obtiznost);
        }
        else{
            //znaky = zapis_znaky(farma.kralici[i], obtiznost);
            // kluci->add<nanogui::Label>(znaky);
            pridej_kralika_do_okna(kotecK,svet.farma.kralici[i], svet.obtiznost);
        }
    }
}
//tato funkce nastavi vlastnosti textu o cilovem kralikovi
void nastav_cil(nanogui::Widget *text){
    std::string znaky = zapis_znaky(SVET.cil,SVET.obtiznost);
     nanogui::Layout *horizontalni_orientace = new nanogui::BoxLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Middle, 0,0);
    text->setLayout(horizontalni_orientace);
    nanogui::Label *nadpis = text->add<nanogui::Label>("Toto je králík kterého chceme vyšlechtit:");
    nanogui::Label *cil = text->add<nanogui::Label>(znaky);
    text->add<nanogui::Label>("");
    nadpis->setFontSize(20);
    cil->setFontSize(25);
}
//vytvori okno farmy a dva wdgety jeden pro kluky a druhy pro holky
//zavola funkci na pridani kralika do widgetu pro kazdeho kralika na farme
void vytvor_okno_farmy(nanogui::Screen * obrazovka, const HRA & svet){
    nanogui::Window *okno = new nanogui::Window(obrazovka, "Farma");
    globalni_okno = okno;
    nanogui::Widget *cil = okno->add<nanogui::Widget>();
    nastav_cil(cil);
    nanogui::Widget *kralici = okno->add<nanogui::Widget>();
    nanogui::Widget *holky = kralici->add<nanogui::Widget>();
    nanogui::Widget *kluci = kralici->add<nanogui::Widget>();
    napln_kotce(holky,kluci,svet);
    //nanogui::CheckBox *holky = kralici->add<nanogui::CheckBox>();
    //nanogui::CheckBox *kluci = kralici->add<nanogui::CheckBox>();
    //nanogui::Button * ulozit = okno->add<nanogui::Button>("Spářit");
   // ulozit->setCallback([=]{zmacknuti_ulozit(holky,kluci,svet);});
    nanogui::Widget *menu = okno->add<nanogui::Widget>();
    nanogui::Layout *horizontalni_orientace = new nanogui::BoxLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Middle, 0,40);
    cil->setLayout(horizontalni_orientace);
    nanogui::Layout *vertikalni_orientace = new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Fill, 0,5);
    okno->setLayout(vertikalni_orientace);
    kralici->setLayout(horizontalni_orientace);
    holky->setLayout(vertikalni_orientace);
    kluci->setLayout(vertikalni_orientace);
    menu->setLayout(vertikalni_orientace);
    //nanogui::Button * tlacitko = menu->add<nanogui::Button>("Konec");
    nanogui::Button * tlacitko2 = menu->add<nanogui::Button>("Hlavní menu");
    //tlacitko->setCallback(zmacknuti_UKONCIT);
    tlacitko2->setCallback(zmacknuti_MENU);
    //napln_kotce(holky,kluci,svet);
    obrazovka->performLayout();
    okno->center();
}
// funkce jde_vytvorit_gen zjisti zda jde vytvorit gen pomoci kralika ktereho dostene
OVER jde_vytvorit_gen(const GEN & rodic,const int & pohlavi, const GEN & potomek, OVER gen){
    if(gen.uspech == true) return gen;
    if(potomek.gen == 0){
        if(pohlavi == 0 && (rodic.gen == 0 || rodic.gen == 1) ){
            gen.otec = 0;
        }
        else if(pohlavi == 1 && (rodic.gen == 0 || rodic.gen == 1)){
            gen.matka = 0;
        }
        if(gen.otec == 0 && gen.matka == 0){
            gen.uspech = true;
        }
    }
    else{
        if(pohlavi == 0){
            gen.otec = rodic.gen;
        }
        else{
            gen.matka = rodic.gen;
        }
        if( (gen.otec == 11) || (gen.matka == 11) || (gen.otec == 1) || (gen.matka == 1)){
            gen.uspech = true;
        }
    }
    return gen;
}
// tato funkce zjisti jestli jsou splneny vsechny podminky v strukture OVEROVAC na true
bool je_vse_true(const OVEROVAC & over){
    if((over.barva.uspech == true) && (over.srst.uspech == true) && (over.ocasek.uspech == true) && (over.usi.uspech == true) && (over.oci.uspech == true)){
        return true;
    }
    else return false;
}
// funkce Jde_kralik_vytvorit zjisti zda jde cilovy kralik vytvorit pomoci kraliku z farmy
//pouziva funkci jde_vyvorit_gen na kazdy jednotlivy gen kralika
bool Jde_kralik_vytvorit(const HRA & hra){
    OVEROVAC over;
    int i = 0;
    while(over.overeno != true && i != hra.farma.pocetK ){
        over.barva = jde_vytvorit_gen(hra.farma.kralici[i].barva, hra.farma.kralici[i].pohlavi, hra.cil.barva, over.barva);
        over.oci = jde_vytvorit_gen(hra.farma.kralici[i].oci, hra.farma.kralici[i].pohlavi, hra.cil.oci, over.oci);
        over.usi = jde_vytvorit_gen(hra.farma.kralici[i].usi, hra.farma.kralici[i].pohlavi, hra.cil.usi, over.usi);
        over.ocasek = jde_vytvorit_gen(hra.farma.kralici[i].ocasek, hra.farma.kralici[i].pohlavi, hra.cil.ocasek, over.ocasek);
        over.srst = jde_vytvorit_gen(hra.farma.kralici[i].srst, hra.farma.kralici[i].pohlavi, hra.cil.srst, over.srst);
        over.overeno = je_vse_true(over);
        i++;
    }
    return over.overeno;
}
// tato funkce vytvori kraliky na farme podle spravne obtiznosti
void vytvor_farmu(int obtiznost){
    SVET.obtiznost = obtiznost;
    SVET.generace = 0;
    SVET.cil = nahodny_kralik(SVET.obtiznost);
    globalni_par.mame_samce = false;
    globalni_par.mame_samici = false;
    int i;
    SVET.farma.pocetK = 5;
    do{
        for(i = 0; i < SVET.farma.pocetK; i++){
            SVET.farma.kralici[i] = nahodny_kralik(SVET.obtiznost);
            if (jsou_kralici_stejni(SVET.farma.kralici[i],SVET.cil)){
                i--; // pokud je kralik stejny jako cilovy kralik tak bude prepsan jinym kralikem
            }
        }
    }while (Jde_kralik_vytvorit(SVET) != true);
}
//funguje pro vsechny calback funkce pro zmacknuti obtiznosti
//vola funkci  vytvor_okno_farmy(obrazovka.screen, SVET)
// vola funkci vytvor_farmu(obtiznost) ktera farmu nakonfiguruje
void zmacknuti_obtiznosti(int obtiznost){
    globalni_okno->dispose();
    globalni_okno = NULL;
    vytvor_farmu(obtiznost);
    vytvor_okno_farmy(obrazovka.screen, SVET);
}
//nastaveni obtiznosti na 1
void zmacknuti_LEHKA(void){
    zmacknuti_obtiznosti(1);
}
//nastaveni obtiznosti na 2
void zmacknuti_STREDNI(void){
    zmacknuti_obtiznosti(2);
}
//nastaveni obtiznosti na 3
void zmacknuti_TEZKA(void){
    zmacknuti_obtiznosti(3);
}
//tato funkce vytvori okno kde se uzivatele pta jakou obtiznost hry chce zvolit
void vytvor_okno_obtiznosti(nanogui::Screen * obrazovka){
    nanogui::Window *okno = new nanogui::Window(obrazovka, "Volba obtížnosti");
    nanogui::Layout *svisla_orientace = new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 8, 4);
    okno->setLayout(svisla_orientace);
    nanogui::Label * text = okno->add<nanogui::Label>("Ocitli jste se v simulaci, která má ukazovat jak funguje genetika a procvičovat jí. Máte farmu, která obsahuje několik králíků. A zadaného jednoho králíka, kterého se snažíte vyšlechtit. Můžete vždy spářit pouze dva králiky různého pohlaví. Každý králík nějak vypadá, ale jeho genetická informace může být jiná, než se zdá. Napřiklad pokud je králík hnědý jeho genetická informace může obsahovat i gen pro bílou barvu, nebo jeho genetická informace je skládána z 2 hnědých genů. U každé vlastnosti králíka je napsáno D pokud je vlastnost dominantní a R pokud je vlastnost recesivní. Tedy pokud bude u králíka napsáno R víte že oba jeho geny jsou recesivní.");
    text->setFixedWidth(500);
    text->setFontSize(20);
    okno->add<nanogui::Label>("Zvolte si, prosím obtížnost");
    nanogui::Widget *tlacitka = okno->add<nanogui::Widget>();
    nanogui::Layout *horizontalni_orientace = new nanogui::BoxLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Middle, 0,40);
    tlacitka->setLayout(horizontalni_orientace);
    nanogui::Button *tlacitko1 = tlacitka->add<nanogui::Button>("LEHKÁ");
    nanogui::Button *tlacitko2 = tlacitka->add<nanogui::Button>("STŘEDNÍ");
    nanogui::Button *tlacitko3 = tlacitka->add<nanogui::Button>("TĚŽKÁ");
    tlacitko1->setCallback(zmacknuti_LEHKA);
    tlacitko2->setCallback(zmacknuti_STREDNI);
    tlacitko3->setCallback(zmacknuti_TEZKA);
    obrazovka->performLayout();
    okno->center();
    globalni_okno = okno;
}
//tato funkce zaridi vstup do hry
void zmacknuti_VSTUP(void){
    globalni_okno->dispose();
    globalni_okno = NULL;
    vytvor_okno_obtiznosti(obrazovka.screen);
}
//tato funkce vytvori hlavni okno kde se uzivatel rozhodne zda vstoupi do hry
//okno obsahuje 2 dlacitka jedno ktere zaridi vstup do hry a druhe ktere ukonci aplikaci
void vytvor_hlavni_okno(nanogui::Screen * obrazovka){
    nanogui::Window *okno = new nanogui::Window(obrazovka, "Hlavní menu");
    nanogui::Layout *svisla_orientace = new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 8, 4);
    okno->setLayout(svisla_orientace);
    nanogui::Label * text =okno->add<nanogui::Label>("Toto je výuková aplikace genetiky zaměřená na studenty středních škol. V této hře si student prakticky ověří své znalosti ze studia genetiky.");
    text->setFixedWidth(500);
    text->setFontSize(20);
    okno->add<nanogui::Label>("Pojďme to zkusit!");
    nanogui::Widget *tlacitka = okno->add<nanogui::Widget>();
    nanogui::Layout *horizontalni_orientace = new nanogui::BoxLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Middle, 0,40);
    tlacitka->setLayout(horizontalni_orientace);
    nanogui::Button *tlacitko1 = tlacitka->add<nanogui::Button>("VSTOUPIT DO HRY");
    nanogui::Button *tlacitko2 = tlacitka->add<nanogui::Button>("UKONČIT");
    tlacitko1->setCallback(zmacknuti_VSTUP);
    tlacitko2->setCallback(zmacknuti_UKONCIT);
    obrazovka->performLayout();
    okno->center();
    globalni_okno = okno;
}
int main(int argc, const char * argv[]) {
    srand(time(NULL));
    if(! obrazovka.create()){
        return 1;
    }
    vytvor_hlavni_okno(obrazovka.screen);
    obrazovka.run();
   // printf("Odehranych her: %d", pocet_her);
    obrazovka.destroy();
    return 0;
}
